<h3>Új adat felvétele</h3>

<?php
	if (isset($_POST['felvesz']))
	{
		$cim = escapeshellcmd($_POST['cim']);
		$leiras = escapeshellcmd($_POST['leiras']);

		if (empty($cim) || empty($leiras))
		{
			showError('Hiba! Nem töltötted ki az adatokat!');
		}
		else
		{
			$db->query("INSERT INTO hirek VALUES(null, '$cim','$leiras',CURRENT_TIMESTAMP, 1)");
			
			header('location: index.php?pg=listazas');
		}
	}
?>

<form method="POST" action="index.php?pg=ujadat">
	<div class="form-group">
		<label for="cim">A Hír rövid címe:</label>
		<input type="text" name="cim" class="form-control">
	</div>
	<div class="form-group">
		<label for="leiras">A hír leírás:</label>
		<textarea id="szerkeszto" name="leiras" class="form-control"></textarea>
	</div>
	<div class="form-group">
		<input type="submit" name="felvesz" value="Hír felvétele" class="btn btn-primary">
	</div>
</form>
<a href="index.php?pg=listazas" class="btn btn-primary">Vissza a hírek listájához...</a>
<br><br>