<h3>Mentési pont létrehozása</h3>
<hr>
<?php
	if (isset($_POST['save']))
	{
		$savepointname = escapeshellcmd($_POST['savepointname']);
		if (empty($savepointname))
		{
			showError('Nem adtál meg nevet!');
		}
		else
		{
			$db->query("SAVEPOINT $savepointname;");
			showSuccess("A mentési pont létrehozva!");

		}
	}
	echo '
<form method="POST" action="index.php?pg=savepoints">
	<div class="form-group">
		<input type="text" name="savepointname" placeholder="Mentési pont neve">
		<input type="submit" name="save" value="Mentés" class="btn btn-primary">
		<a href="index.php?pg=listazas" class="btn btn-primary">Vissza a hírek listájához...</a>
	</div>
</form>';

?>