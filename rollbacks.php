<h3>Visszaállítási pontok</h3>
<hr>
<?php
	if (isset($_POST['rollback']))
	{
		$rollbackname = escapeshellcmd($_POST['rollbackname']);
		if (empty($rollbackname))
		{
			showError('Nem adtál meg nevet!');
		}
		else
		{
			$db->query("ROLLBACK TO $rollbackname;");
			showSuccess("A visszaállítás sikeres!");
		}
	}
	echo '
<form method="POST" action="index.php?pg=rollbacks">
	<div class="form-group">
		<input type="text" name="rollbackname" placeholder="Visszaállítási pont neve">
		<input type="submit" name="rollback" value="Visszaállít" class="btn btn-primary">
		<a href="index.php?pg=listazas" class="btn btn-primary">Vissza a hírek listájához...</a>
	</div>
</form>';

?>

