-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Gép: localhost
-- Létrehozás ideje: 2020. Már 13. 12:09
-- Kiszolgáló verziója: 10.4.6-MariaDB
-- PHP verzió: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `214SZF_projekt1`
--
CREATE DATABASE IF NOT EXISTS `214SZF_projekt1` DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci;
USE `214SZF_projekt1`;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `felhasznalok`
--

CREATE TABLE `felhasznalok` (
  `ID` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `jelszo` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `regdatum` datetime NOT NULL,
  `utbelep` datetime NOT NULL,
  `statusz` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `felhasznalok`
--

INSERT INTO `felhasznalok` (`ID`, `nev`, `email`, `jelszo`, `regdatum`, `utbelep`, `statusz`) VALUES
(1, 'Oldal Adminisztrátor', 'admin@projekt1.hu', 'e10adc3949ba59abbe56e057f20f883e', '2019-10-11 00:00:00', '2019-10-11 00:00:00', 1),
(2, 'teszt1', 'teszt1@webprojekt.hu', '0cc175b9c0f1b6a831c399e269772661', '2019-10-16 15:58:42', '2019-10-16 15:58:42', 1),
(3, 'teszt2', 'teszt2@webprojekt.hu', '92eb5ffee6ae2fec3ad71c777531578f', '2019-10-16 16:00:47', '2019-10-16 16:00:47', 1),
(4, 'teszt3', 'teszt3@webprojekt.hu', '4a8a08f09d37b73795649038408b5f33', '2019-10-16 16:15:03', '2019-10-16 16:15:03', 1),
(5, 'teszt4', 'teszt4@webprojekt.hu', '0cc175b9c0f1b6a831c399e269772661', '2019-10-16 16:16:00', '2019-10-16 16:16:00', 2),
(6, 'teszt5', 'teszt5@webprojekt.hu', '0cc175b9c0f1b6a831c399e269772661', '2019-10-16 16:16:39', '2019-10-16 16:16:39', 0),
(7, 'teszt6', 'tesz6@teszt.hu', 'c5d697504ef90769bf5dcb24f8b23651', '2019-10-21 14:44:22', '2019-10-21 14:44:22', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE `hirek` (
  `ID` int(11) NOT NULL,
  `cim` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `felhID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `hirek`
--

INSERT INTO `hirek` (`ID`, `cim`, `tartalom`, `datum`, `felhID`) VALUES
(1, 'Elkészült a weboldalunk', 'asjd fhjsdg fjhadsg fhj asdfags djfga sdhfg asjdf , gsdhfhjasgd fjha gshfg asd fgas gfa,hsdg f,jagsd f,jh sdagf,jhas gdfhjags dfg asd,hg as,jdf gasd,h fgas, gf, fghj,dgf a,fg hdgf hsdgfj hdf,ksa', '2019-10-11 17:00:00', 1),
(2, 'Ez a második hír', 'dfn jsdhfjkas hdkjfah sd.f, hasd.kf asd,jfha,sm fda, dfaj,sd fjhsd fjkas hdfkj asdfjk asd,fh asd,jf asd.kjf a.ksdjhf a.sd fklasdhnfa.sdh f.kjasdhf', '2019-10-14 00:00:00', 1),
(3, 'Ez meg a harmadik hír', 'dfgsdfgb s,djkgbsdkfg sdf\r\n gsdfg sdfgsdfjkg hasfkjg bsdfjkas djkfh,as dfjsakgdf,jhsd g,jh asgvdjHDSFV ,JHASDf,hj GFH GFAJS,DGFJH DSGHDSG FJHAGSDF,JH SAGDF,JA S,JDFG AS,JDHFSKJDF HGSKDJFHGSDKJ HG,JSDHF,AJSD', '2019-10-14 08:00:00', 1),
(4, 'Most már lehet új hírt felvenni', 'waedfam sdnf asdhg fhgsd fjhasd f,hjas dfasdfsad', '2019-10-14 00:00:00', 1),
(5, 'Ez a legújabb hír', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa a   aaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaa\r\n			\r\n			\r\n			', '2019-10-14 19:00:23', 1),
(8, 'ez egy új hír', '<p style=\"text-align: left;\"><strong>idfhgsdfgsdfgsdfg</strong> as <span style=\"text-decoration: line-through; color: #ba372a;\"><em>fsdgsdgsdfgsdfg</em></span> sdf gsdfg sdf gs</p>', '2019-11-04 14:53:24', 1),
(9, 'sdfasdfasdfasdf', 'sdfasdfasdf', '2020-02-26 15:42:19', 1),
(10, 'fsdfsdfsf', 'sdfsdfds', '2020-02-26 16:07:44', 1),
(11, 'werwer', 'werwerwere', '2020-02-26 16:07:53', 1),
(12, 'yxcadfasf', 'asdfasdf', '2020-02-26 16:09:43', 1),
(13, 'dsfgsfg', 'sdfgsdfgsdfg', '2020-02-26 16:09:56', 1),
(14, 'dgsfsdfgsd', 'fgsdfgsdfgsfg', '2020-02-26 16:10:00', 1),
(15, 'serdfsdfgdfgs', 'dretrtgsergewrsgfg', '2020-02-26 16:10:09', 1),
(16, 'wqerqerqwer', 'qwerqwerqwer', '2020-02-26 16:11:39', 1),
(17, 'werqerqwer', 'qwerqwerqw', '2020-02-26 16:11:52', 1),
(18, 'asdfasdfasdfa', 'sdfasdfasdfasdf', '2020-02-26 16:15:53', 1),
(19, 'asdfasdfasdf', 'asdfasdfasdf', '2020-02-26 16:15:59', 1),
(20, 'sdfasdfasgrterter', 'ertwrtwert', '2020-02-26 16:16:06', 1),
(21, 'wqeffasdf', 'asdfasdfsdfasdfass', '2020-02-26 16:16:55', 1),
(22, 'sdfasdfas', 'asdfasdfds', '2020-02-26 16:24:21', 1),
(23, 'sadfasdfadf', 'asdfasdfasdf', '2020-02-28 15:25:37', 1),
(24, 'sdfadfasdf', 'asdfasdf', '2020-02-28 15:25:46', 1),
(25, 'dsfgsdfgdsfg', 'sdfgsdfgdsfg', '2020-03-13 12:05:55', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termekek`
--

CREATE TABLE `termekek` (
  `ID` int(11) NOT NULL,
  `kategoria` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `termeknev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `netto` double NOT NULL,
  `afa` double NOT NULL,
  `menny` double NOT NULL,
  `egyseg` varchar(10) COLLATE utf8_hungarian_ci NOT NULL,
  `gar` int(11) NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `termekkep` varchar(100) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `termekek`
--

INSERT INTO `termekek` (`ID`, `kategoria`, `termeknev`, `netto`, `afa`, `menny`, `egyseg`, `gar`, `leiras`, `termekkep`) VALUES
(1, 'Szerszám', 'Talicska', 5000, 1.27, 10, 'db', 6, 'ydxsdfasd fsafasdf asdfasdf ', '1.png'),
(2, 'Szerszám', 'Ásó', 2000, 1.27, 100, 'db', 3, 'dfsa df asd fasdf asdf asdf asd', ''),
(3, 'Szerszám', 'Kalapács', 1500, 1.27, 50, 'db', 1, 'yxgd asf afdg sdfg sdf gsdf gsdg', ''),
(4, 'Gép', 'Traktor', 2500000, 1.27, 2, 'db', 12, 'asdfadfaasgfdsgsdf', '4.png'),
(5, 'Gép', 'Fűnyíró', 200000, 1.27, 8, 'db', 36, 'wqerqwer\r\nqwerqwer\r\nqwerq\r\nwerqwerqwerweqrqer', ''),
(7, 'bbbb', 'bbbb', 1000, 1.27, 99, 'm', 3, 'bbbbbbbbbbbbbbb', '');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `termekek`
--
ALTER TABLE `termekek`
  ADD PRIMARY KEY (`ID`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `felhasznalok`
--
ALTER TABLE `felhasznalok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT a táblához `termekek`
--
ALTER TABLE `termekek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
