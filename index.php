<!DOCTYPE html>
<?php
	session_start();
	require("adatok.php");
	require("fuggvenytar.php");
	require("database.php");
	// itt hozzuk létre a saját adatbázis objektumunkat $db néven
	$db = new db($dbhost, $dbuser, $dbpass, $dbname);
?>
<html>
<head>
	<meta charset="utf-8">	
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="css/transaction.css">
	

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>

</head>
<body>
	<div class="container" id="container">
		 <header class="col-xs-12">
		 	<?php echo '<a href="index.php">'.$title.'</a>'; ?>
		 </header>
		 <section class="col-xs-12">
		 	<article class="col-xs-12 col-sm-3 shadow">
		 		<?php 
					include("menu.php");
		 		?>
		 	</article>
		 	<article class="col-xs-12 col-sm-9">
		 		<?php include("betolto.php"); ?>
		 	</article>
		 </section>
		 <footer class="col-xs-12">
		 	<?php echo $company.' - '.$author; ?>
		 </footer>
	</div>
	<script type="text/javascript" src="js/lightbox.js"></script>
</body>
</html>